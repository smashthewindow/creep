# Scrapy settings for creeper project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

import os

BOT_NAME = 'creeper'

SPIDER_MODULES = ['creeper.spiders']
NEWSPIDER_MODULE = 'creeper.spiders'

USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36'
DOWNLOAD_DELAY = 2    # 250 ms of delay

EXTENSIONS = {
   'scrapy.telnet.TelnetConsole': None,
   'scrapy.webservice.WebService': None
}