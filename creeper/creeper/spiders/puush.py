import re, StringIO, sys, cv2, numpy, tweetpony
from PIL import Image
from scrapy.spider import BaseSpider
from scrapy.http import Request
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from creeper.items import CreeperItem

api = tweetpony.API(consumer_key="XY8GM0jUrtL4JqYhxeeQA",consumer_secret="lLKofmIHpiVrYZkEHHp5wRmDdjds1iRUR7fohJtAN1o", access_token="1967935118-1D2BrNyGFNnIhl706vr42Bo8Abdg2YQbtYSwET1", access_token_secret="jso7GZBuT0glLBMLPCdHHPt02juoaSSYJAKTgwpxcto")
body_cascade = cv2.CascadeClassifier('/Users/iljaelee/Documents/Crawler/creep/creeper/creeper/spiders/fullbody.xml')

def get_skin_ratio(im):
    im = im.crop((int(im.size[0]*0.2), int(im.size[1]*0.2), im.size[0]-int(im.size[0]*0.2), im.size[1]-int(im.size[1]*0.2)))
    skin = sum([count for count, rgb in im.getcolors(im.size[0]*im.size[1]) if rgb[0]>60 and rgb[1]<(rgb[0]*0.85) and rgb[2]<(rgb[0]*0.7) and rgb[1]>(rgb[0]*0.4) and rgb[2]>(rgb[0]*0.2)])
    return float(skin)/float(im.size[0]*im.size[1])

def detect(im):
    rgb_image = im
    file_bytes = numpy.array(rgb_image.convert('RGB'))
    frame = file_bytes[:, :, ::-1].copy()
    
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    frame_gray = cv2.equalizeHist(frame_gray)
    
    bodies = body_cascade.detectMultiScale(image=frame_gray, scaleFactor=1.1, minNeighbors=2, flags=0|cv2.CASCADE_SCALE_IMAGE, minSize=(30,30))
    if len(bodies) == 0:
        return False

    skin_percent = get_skin_ratio(im) * 100
    if skin_percent > 30:
        return True

    return False

#scrapy crawl puush -a start_url=http://puu.sh/1BJGS

ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

def base62_encode(num, alphabet=ALPHABET):
    """Encode a number in Base X

    `num`: The number to encode
    `alphabet`: The alphabet to use for encoding
    """
    if (num == 0):
        return alphabet[0]
    arr = []
    base = len(alphabet)
    while num:
        rem = num % base
        num = num // base
        arr.append(alphabet[rem])
    arr.reverse()
    return ''.join(arr)

def base62_decode(string, alphabet=ALPHABET):
    """Decode a Base X encoded string into the number

    Arguments:
    - `string`: The encoded string
    - `alphabet`: The alphabet to use for encoding
    """
    base = len(alphabet)
    strlen = len(string)
    num = 0

    idx = 0
    for char in string:
        power = (strlen - (idx + 1))
        num += alphabet.index(char) * (base ** power)
        idx += 1

    return num

max_size = 400, 400

class PuushSpider(BaseSpider):
    count = 0
    name = 'puush'
    allowed_domains = ['puu.sh']
    handle_httpstatus_list = [404]

    def __init__(self, *args, **kwargs): 
      super(PuushSpider, self).__init__(*args, **kwargs) 
      self.start_urls = [kwargs.get('start_url')] 

    def parse(self, response):
        if response.status == 200 and 'image' in response.headers['Content-Type']:
            try:
                file = StringIO.StringIO(response.body)
                image = Image.open(file)
                if detect(image):
                    url_status = "Porn found: " + response.url
                    api.update_status(status=url_status)
            except:
                print "Unexpected error:", sys.exc_info()[0]
                
        number = base62_decode(re.split('^(.*[\\\/])', response.url)[-1])
        request_url = 'http://puu.sh/'+base62_encode(number+1)
        yield Request(url=request_url)